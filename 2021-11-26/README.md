Expiriment details:

Sweep of Arc: 100 degrees to 200 degrees
~10 degree intervals. 
100 Images

Output Gif: 

![output gif](./output.gif)

Stellarium screen shot of sky captured. 

![stellarium](./stellarium.png)
