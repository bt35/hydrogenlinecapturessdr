#!/bin/bash 


K=0
while true;
do 
	rtl_power_fftw -f 1420405752 -t 10 -b 512 -g 420 -B baseline |\
	      	gnuplot -e "set term png; set timestamp ; set title '24 Hour Capture - Baseline Subtraction' ;unset key; set xtics rotate by -90; plot '-' w l" > spectrums/spectrum-$K.png
	let "K+=1"
	spd-say "Turn"
	echo "Iteration $K"
	sleep 1
done
