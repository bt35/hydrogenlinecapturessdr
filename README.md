# HydrogenLineCapturesSDR

2.4 GHz Wire Dish Antenna with a Nooelec SAWBird H1 LNA connected to Nooelec Smartee XTR poorly done drift scans with rtl_power_fftw and Gnu Plot. 

# Method

I live in a very noisy location and very busy. Therefore instead of doing a 24 hour drift scan or longer I have to resort to holding the antenna in my hand and sweeping it back and forth over one of the spiral arms of the galaxy. The method is a bit like this: 

1. Make a baseline using rtl_power_fftw and a 50 ohm terminator for noise subtraction. 
2. Take antenna outside at around 19 to 20 hours (7 to 8 local time CST), which coincides with Altair, Venus, and Rasalhague passing by. Jupiter, Saturn, Venus, and Vega are out there too. 
2. Turn on the 24 hour timelapse script and move the antenna left 5 time and right 5 times a total of 4 times for 20 captures. This should give us two left to right scans of the dopller shift of the Hydrogen Line. 
```
# Added a text to speech notification that we should turn the antenna on every interval if we are making drift map. 
# Todo? Put the antenna on a rotating base and make it turn automatically via some sort of mechengical contrivance. 
# take in posiutional arguments to name the graph instead of hardocoding the name. 
#!/bin/bash 

DESCRIPTION=$1

K=0
while true;
do 
	rtl_power_fftw -f 1420405752 -t 10 -b 512 -g 420 -B baseline |\
	      	gnuplot -e "set term png; set timestamp ; set title '$DESCRIPTION - Baseline Subtraction' ;unset key; set xtics rotate by -90; plot '-' w l" > Spectrums/spectrum-$K.png
	let "K+=1"
	spd-say "Turn"
	# Automate a servo to turn a turn table at X angle increments... to further automated the sweep of sky. 
	# Current turn table requires manual actuation. 
	echo "Iteration $K"
	sleep 1
done



```
3. The script will run in intervals when ever the next iteration starts move the antenna to the next view point, like 5 to 10 degress to the right. Do that 5 times ot the right, then 5 times to the left, then 5 times to the right and five times to the left. This should result in 20 plots and therefore a couple of oscillations of the doppler shift of the captured hydrogen spectra from the spiral arm. (I do not know the right name for this arm but it is nestled between Altair and Vega. )

# Results? 

Make a gif out of data PNGs. 

```
 convert -delay 60 loop 1 -layers Optimize $(ls -1 *.png | sort -V) output.gif
```

![GIF](2021-11-26/output.gif)

Here is a capture of about 125 observations from about 180° to 0° Azimuth at about 50° Elevation. I have decreased the delay between observations to about 10 so that the animation is faster, and we can more clearly see the doppler shift. 

![GIF](1800output.gif)

180° to 0° Azimuth at about 50° Elevation would have captured about this part of the sky: 

![Stellarium](stellarium.png)

# Virgo Method

The script below is the Virgo Example with Cygnus A as the object of observation. 

We have a for loop that will run 100 times and in this case take 100 pictures. I have added the spd-say command to tell me when to turn my antenna 0 degrees to 180 degrees (North to South), in 5 turn intervals. The goal was to map the doppler shift by looking at different parts of the Cygnus section of the sky. 

```
import virgo
import os

def doThing(obsNum):

    # Define observation parameters
    obs = {
        'dev_args': '',
        'rf_gain': 30,
        'if_gain': 20,
        'bb_gain': 18,
        'frequency': 1420e6,
        'bandwidth': 2.4e6,
        'channels': 2048,
        't_sample': 1,
        'duration': 60,
        'loc': '',
        'ra_dec': '',
        'az_alt': ''
    }

    # Check source position
   # virgo.predict(lat=32.9, lon=-96.8, source='Cas A', date='2021-12-03')

    # Begin data acquisition
    virgo.observe(obs_parameters=obs, obs_file='observation.dat')

    # Analyze data, mitigate RFI and export the data as a CSV file
    # The calibrationfile used here was created by making an observation with the radio disconnected from the antenna.
    virgo.plot(obs_parameters=obs, n=20, m=35, f_rest=1420.4057517667e6, avg_ylim=(-5,15), cal_ylim=(-20,260), meta=False, vlsr=False,cal_file="off.dat",
               obs_file='observation.dat', rfi=[(1419.2e6, 1419.3e6), (1420.8e6, 1420.9e6)],
               dB=True, spectra_csv='baseline', plot_file='plot-'+str(obsNum)+'-noisefloorcal.png')

# Make 100 Observations to make a big dig
for i in range(100):
    doThing(i)
    os.system("""echo "Turn!" | spd-say -e """)
    print("Iteration ----- {} -----".format(i))

```

Here is a stand alone capture of cygnus using Virgo's calibration. The little bump to the left of the Spectral Line is what we were looking for. 

![Single Cygnus Capture](Cygnus.png)

Here is a gif created from the images captured using the script from above. This Virgo catpure is very nice as it results in a very good plot. Which means we can see the Doppler shift in a much smoother view. 

![Gif](cygnus.gif) 

# Comparing Virgo and RTL_FFTW_Captures:

Antenna was orientated toward 305 degrees at 40 ish degrees elevation. In other words looking roughly at the sky where Deneb is. 

![Virgo Deneb](virgoDeneb.png)

![RTL Power FFTW Deneb](rtl-powerfftw-deneb.png)


# Improvements to equipment and method? 
- []  Make a platform that will turn automatically. 
- []  Put a motor on that platform
- []  Make the timelapse code turn the motor to perform the sweeps at a much more controlled interval with more accuracy. This should result in much better oscillation detection (drift scan output)
- [x] Make a simular application using Virgo. 

# Reference for Tools: 

* https://github.com/AD-Vega/rtl-power-fftw
* http://kmkeen.com/rtl-power/
* https://github.com/keenerd/rtl-sdr
* https://github.com/byggemandboesen/H-line-software
* https://github.com/nategri/simple-rpi-hydrogen-radio
* https://github.com/0xCoto/Virgo
