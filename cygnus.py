import virgo
import os

def doThing(obsNum):

    # Define observation parameters
    obs = {
        'dev_args': '',
        'rf_gain': 30,
        'if_gain': 20,
        'bb_gain': 18,
        'frequency': 1420e6,
        'bandwidth': 2.4e6,
        'channels': 2048,
        't_sample': 1,
        'duration': 60,
        'loc': '',
        'ra_dec': '',
        'az_alt': ''
    }

    # Check source position
   # virgo.predict(lat=32.9, lon=-96.8, source='Cas A', date='2021-12-03')

    # Begin data acquisition
    virgo.observe(obs_parameters=obs, obs_file='observation.dat')

    # Analyze data, mitigate RFI and export the data as a CSV file
    # The calibrationfile used here was created by making an observation with the radio disconnected from the antenna. 
    virgo.plot(obs_parameters=obs, n=20, m=35, f_rest=1420.4057517667e6, avg_ylim=(-5,15), cal_ylim=(-20,260), meta=False, vlsr=False,cal_file="off.dat",
               obs_file='observation.dat', rfi=[(1419.2e6, 1419.3e6), (1420.8e6, 1420.9e6)],
               dB=True, spectra_csv='baseline', plot_file='plot-'+str(obsNum)+'-noisefloorcal.png')

# Make 100 Observations to make a big dig
for i in range(100):
    doThing(i)
    os.system("""echo "Turn!" | spd-say -e """)
    print("Iteration ----- {} -----".format(i))
